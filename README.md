# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
This repository contains the work output as a result of a technical exercise.
* Version
Symfony 5.3.6
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
Some updates and/or dependencies may be required to install to make project work.
* Dependencies
Symfony 5
Symfony Console Component for CLI commands
PHP Unit
* How to run tests
Project Utilises PHPUnit Tests with Symfony Integration
Execute a unit test by typing on the console  'php ./vendor/bin/phpunit .\tests\[TESTFILENAME].php'


* Deployment instructions
Execute Export command by typing 'php/bin/console.php export' in the console to export data and create a csv file

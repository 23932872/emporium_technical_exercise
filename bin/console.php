#!/usr/bin/env php

<?php
require_once __DIR__ . '/../vendor/autoload.php';

use Console\App\Command\FileExportCommand;
use Symfony\Component\Console\Application;

$app = new Application();

// Calls the FileExportCommand
$app->add(new FileExportCommand());

$app->run();
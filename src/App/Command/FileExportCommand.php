<?php

namespace Console\App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;


class FileExportCommand extends Command
{
    protected function configure()
    {
        // The configure section sets the command name (export) its description and an explanation of what it does
        $this
            ->setName('export')
            ->setDescription('Exports Data into a CSV file')
            ->setHelp('This command executes an action that exports specified data to a csv file');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        // The filename variable is used for creating and downloading the csv file once a command is executed
        $filename = 'payments.csv';

        // The list array creates the columns and rows of the required data
        $payments = array(
            // These are the columns
            array('Period', 'Basic_Payment', 'Bonus_Payment'),
            // These are the rows
            // Specific PHP functions for assigning dates have been implemented in the following rows according the technical requirements
            array(date('M/y'), date('Y-m-d', strtotime('last day of this month')), date('Y-m-d', strtotime('+9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+1 month')), date('Y-m-d', strtotime( '+1 month last day of this month')), date('Y-m-d', strtotime('+1 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+2 month')), date('Y-m-d', strtotime( '+2 month last day of this month')), date('Y-m-d', strtotime('+1 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+3 month')), date('Y-m-d', strtotime( '+3 month last day of this month')), date('Y-m-d', strtotime('+2 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+4 month')), date('Y-m-d', strtotime( '+4 month last day of this month')), date('Y-m-d', strtotime('+3 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+5 month')), date('Y-m-d', strtotime( '+5 month last day of this month')), date('Y-m-d', strtotime('+4 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+6 month')), date('Y-m-d', strtotime( '+6 month last day of this month')), date('Y-m-d', strtotime('+5 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+7 month')), date('Y-m-d', strtotime( '+7 month last day of this month')), date('Y-m-d', strtotime('+6 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+8 month')), date('Y-m-d', strtotime( '+8 month last day of this month')), date('Y-m-d', strtotime('+7 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+9 month')), date('Y-m-d', strtotime( '+9 month last day of this month')), date('Y-m-d', strtotime('+8 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+10 month')), date('Y-m-d', strtotime( '+10 month last day of this month')), date('Y-m-d', strtotime('+9 month +9 days', strtotime('first day of this month')))),
            array(date('M/y', strtotime('+11 month')), date('Y-m-d', strtotime( '+11 month last day of this month')), date('Y-m-d', strtotime('+10 month +9 days', strtotime('first day of this month')))),
        );

        // The section below writes the array date from the payments variable for the csv file.
        $file = fopen($filename,"w");

        foreach ($payments as $line){
            fputcsv($file,$line);
        }

        fclose($file);

        // This section implements the code for reading and downloading the file which should implement it on the application directory
        header("Content-Description: File Transfer");
        header("Content-Disposition: attachment; filename=".$filename);
        header("Content-Type: application/csv; ");

        readfile($filename);
    }
}


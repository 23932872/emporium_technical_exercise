<?php

use \PHPUnit\Framework\TestCase;

class FileAssertionTest extends TestCase
{
    public function testPositiveTestcaseForAssertFileExists()
    {
        $filename = "payments.csv";

        // Assert function to test whether given
        // file name exists or not
        $this->assertFileExists(
            $filename,
            "given filename doesn't exist"
        );
    }
}
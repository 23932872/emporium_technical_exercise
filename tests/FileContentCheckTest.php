<?php

use \PHPUnit\Framework\TestCase;

class FileContentCheckTest extends TestCase {
    public function testForAssertFileContent()
    {
        $filename = "payments.csv";

        // Assert function to test whether given
        // file path exists or not
        $this->assertFileExists(
            $filename,
            "given filename doesn't exist"
        );

        // Checks if file has content to prove array content has been written into csv file
        $this->assertStringNotEqualsFile(
            $filename, '');
        $this->assertStringEqualsFile($filename, 'Period');
        $this->assertStringEqualsFile($filename, 'Basic_Payment');
        $this->assertStringEqualsFile($filename, 'Bonus_Payment');

    }
}
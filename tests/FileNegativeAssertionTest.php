<?php

use \PHPUnit\Framework\TestCase;

class FileNegativeAssertionTest extends TestCase
{
    public function testNegativeTestcaseForAssertFileExists()
    {
        $filename = "payments.csv";

        // Assert function to test whether given
        // file path exists or not
        $this->assertFileExists(
            $filename,
            "given filename doesn't exist"
        );
    }
}